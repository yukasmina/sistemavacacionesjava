/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author yukas
 * @param <T>
 */
public class DaoAdaptador<T> implements InterfazDao<T> {

    public Session getSession() {
        return NewHibernateUtil.getSessionFactory().openSession();
    }

    @Override
    public boolean guardar(T obj) {
        System.out.println("LLega para guardar datos");
        boolean b = false;
        Session s = getSession();
        try {
            s.beginTransaction();
            s.saveOrUpdate(obj);
            s.getTransaction().commit();
            b = true;
        } catch (HibernateException e) {
            s.getTransaction().rollback();
            System.out.println("error: "+e);
        }
        return b;
    }

    @Override
    public boolean eliminar(T obj) {
        boolean b = false;
        Session s = getSession();
        try {
            s.beginTransaction();
            s.delete(obj);
            s.getTransaction().commit();
            b = true;
        } catch (HibernateException e) {
            s.getTransaction().rollback();
        }

        return b;
    }

    @Override
    public List<T> enlistar(Class clase) {
        try {
            Criteria cri = getSession().createCriteria(clase);
            List lst = cri.list();
            return lst;
        } catch (HibernateException e) {
            System.out.println(e);
            return new ArrayList();
        }
    }

    @Override
    public T obtenerPorId(Long id, Class clase) {
        Session s = getSession();

        try {
            org.hibernate.Query q = s.createQuery("from " + clase.getName() + " where codigo = ?");
            q.setLong(0, id);
            return (T) q.uniqueResult();
        } catch (HibernateException e) {
            System.out.println(e);
            return null;
        }
    }

}
