/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Servicios;

import Controlador.DaoAdaptador;
import java.util.List;
import Modelo.Licencia;

/**
 *
 * @author yukas
 */
public class ServicioLicencia {
    private DaoAdaptador dao;

    public ServicioLicencia() {
        dao = new DaoAdaptador();
    }

    public boolean guardar(Licencia l) {
        return dao.guardar(l);
    }

    public boolean eliminar(Licencia l) {
        return dao.eliminar(l);
    }
    
    public Licencia getById(Long id){
        return (Licencia) dao.obtenerPorId(id, Licencia.class);
    }
    public List<Licencia> getAll(){
        return dao.enlistar(Licencia.class);
    }
    
}
