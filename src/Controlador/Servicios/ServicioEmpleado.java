/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Servicios;

import Controlador.DaoAdaptador;
import java.util.List;
import Modelo.Empleado;

/**
 *
 * @author yukas
 */
public class ServicioEmpleado {
    private DaoAdaptador dao;

    public ServicioEmpleado() {
        dao = new DaoAdaptador();
    }

    public boolean guardar(Empleado e) {
        return dao.guardar(e);
    }

    public boolean eliminar(Empleado e) {
        return dao.eliminar(e);
    }
    
    public Empleado getById(Long id){
        return (Empleado) dao.obtenerPorId(id, Empleado.class);
    }
    public List<Empleado> getAll(){
        return dao.enlistar(Empleado.class);
    }
    
}
