/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador;

import java.util.List;

/**
 *
 * @author yukas
 * @param <T>
 */
public interface InterfazDao<T> {
    public boolean guardar(T obj);
    public boolean eliminar(T obj);
    public List<T> enlistar(Class clase);
    public T obtenerPorId(Long id,Class clase);
    
}
