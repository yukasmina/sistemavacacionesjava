/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author yukas
 */
@Entity
@Table(name = "licencia")
/*@NamedQueries({
    @NamedQuery(name = "Licencia.findAll", query = "SELECT l FROM Licencia l"),
    @NamedQuery(name = "Licencia.findByIdLicencia", query = "SELECT l FROM Licencia l WHERE l.idLicencia = :idLicencia"),
    @NamedQuery(name = "Licencia.findByTipoList", query = "SELECT l FROM Licencia l WHERE l.tipoList = :tipoList"),
    @NamedQuery(name = "Licencia.findByDGanados", query = "SELECT l FROM Licencia l WHERE l.dGanados = :dGanados"),
    @NamedQuery(name = "Licencia.findByDTomados", query = "SELECT l FROM Licencia l WHERE l.dTomados = :dTomados"),
    @NamedQuery(name = "Licencia.findByDRestantes", query = "SELECT l FROM Licencia l WHERE l.dRestantes = :dRestantes"),
    @NamedQuery(name = "Licencia.findByFInicio", query = "SELECT l FROM Licencia l WHERE l.fInicio = :fInicio"),
    @NamedQuery(name = "Licencia.findByFFin", query = "SELECT l FROM Licencia l WHERE l.fFin = :fFin"),
    @NamedQuery(name = "Licencia.findByHInicio", query = "SELECT l FROM Licencia l WHERE l.hInicio = :hInicio"),
    @NamedQuery(name = "Licencia.findByHFin", query = "SELECT l FROM Licencia l WHERE l.hFin = :hFin"),
    @NamedQuery(name = "Licencia.findByNhAcumuladas", query = "SELECT l FROM Licencia l WHERE l.nhAcumuladas = :nhAcumuladas"),
    @NamedQuery(name = "Licencia.findByObservacion", query = "SELECT l FROM Licencia l WHERE l.observacion = :observacion")})*/
public class Licencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codigo")
    private Integer idLicencia;
    @Column(name = "tipo_list")
    private String tipoList;
    @Basic(optional = false)
    @Column(name = "d_ganados")
    private int dGanados;
    @Column(name = "d_tomados")
    private Integer dTomados;
    @Basic(optional = false)
    @Column(name = "d_restantes")
    private int dRestantes;
    @Column(name = "f_inicio")
    @Temporal(TemporalType.DATE)
    private Date fInicio;
    @Column(name = "f_fin")
    @Temporal(TemporalType.DATE)
    private Date fFin;
    @Column(name = "h_inicio")
    @Temporal(TemporalType.TIME)
    private Date hInicio;
    @Column(name = "h_fin")
    @Temporal(TemporalType.TIME)
    private Date hFin;
    @Column(name = "nh_acumuladas")
    private Integer nhAcumuladas;
    @Column(name = "observacion")
    private String observacion;
    @JoinColumn(name = "codigo", referencedColumnName = "codigo", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Empleado empleado;

    public Licencia() {
    }

    public Licencia(Integer idLicencia) {
        this.idLicencia = idLicencia;
    }

    public Licencia(Integer idLicencia, int dGanados, int dRestantes) {
        this.idLicencia = idLicencia;
        this.dGanados = dGanados;
        this.dRestantes = dRestantes;
    }

    public Integer getIdLicencia() {
        return idLicencia;
    }

    public void setIdLicencia(Integer idLicencia) {
        this.idLicencia = idLicencia;
    }

    public String getTipoList() {
        return tipoList;
    }

    public void setTipoList(String tipoList) {
        this.tipoList = tipoList;
    }

    public int getDGanados() {
        return dGanados;
    }

    public void setDGanados(int dGanados) {
        this.dGanados = dGanados;
    }

    public Integer getDTomados() {
        return dTomados;
    }

    public void setDTomados(Integer dTomados) {
        this.dTomados = dTomados;
    }

    public int getDRestantes() {
        return dRestantes;
    }

    public void setDRestantes(int dRestantes) {
        this.dRestantes = dRestantes;
    }

    public Date getFInicio() {
        return fInicio;
    }

    public void setFInicio(Date fInicio) {
        this.fInicio = fInicio;
    }

    public Date getFFin() {
        return fFin;
    }

    public void setFFin(Date fFin) {
        this.fFin = fFin;
    }

    public Date getHInicio() {
        return hInicio;
    }

    public void setHInicio(Date hInicio) {
        this.hInicio = hInicio;
    }

    public Date getHFin() {
        return hFin;
    }

    public void setHFin(Date hFin) {
        this.hFin = hFin;
    }

    public Integer getNhAcumuladas() {
        return nhAcumuladas;
    }

    public void setNhAcumuladas(Integer nhAcumuladas) {
        this.nhAcumuladas = nhAcumuladas;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLicencia != null ? idLicencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Licencia)) {
            return false;
        }
        Licencia other = (Licencia) object;
        if ((this.idLicencia == null && other.idLicencia != null) || (this.idLicencia != null && !this.idLicencia.equals(other.idLicencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Licencia[ codigo=" + idLicencia + " ]";
    }
    
}
